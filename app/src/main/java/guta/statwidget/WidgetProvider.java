package guta.statwidget;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;



public class WidgetProvider extends AppWidgetProvider {
    public static final String DATA_FETCHED = "guta.statwidget.DATA_FETCHED";
    public static final String ACTION_LEFT_BUTTON_CLICKED = "guta.statwidget.ACTION_LEFT_BUTTON_CLICKED";
    public static final String ACTION_RIGHT_BUTTON_CLICKED = "guta.statwidget.ACTION_RIGHT_BUTTON_CLICKED";


    public static final String EXTRA_ITEM = "guta.statwidget.EXTRA_ITEM";

    public static final String TAG = "WidgetProvider";


    @Override
    public void onEnabled(Context context) {
    }


    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.i(TAG, "onUpdate");
        int appWidgetId;
        for (int i = 0; i < appWidgetIds.length; i++) {
            appWidgetId = appWidgetIds[i];

            Intent serviceIntent = new Intent(context, RemoteFetchService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            context.startService(serviceIntent);

            RemoteViews views = getCurrentLayout(context, appWidgetManager, appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }





    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);



        if (intent.getAction().equals(DATA_FETCHED)) {
            Log.i(TAG, "onReceive DATA_FETCHED");
        }
        else if (intent.getAction().equals(ACTION_LEFT_BUTTON_CLICKED)) {
            if (RemoteFetchService.currentItem > 0) {
                --RemoteFetchService.currentItem;
            } else RemoteFetchService.currentItem = RemoteFetchService.oneList.size() - 1;
            Log.i(TAG, "onReceive ACTION_LEFT_BUTTON_CLICKED RemoteFetchService.currentItem = " + RemoteFetchService.currentItem);
        }
        else if (intent.getAction().equals(ACTION_RIGHT_BUTTON_CLICKED)) {
            RemoteFetchService.currentItem = (RemoteFetchService.currentItem + 1) % RemoteFetchService.oneList.size();
            Log.i(TAG, "onReceive ACTION_RIGHT_BUTTON_CLICKED RemoteFetchService.currentItem = " + RemoteFetchService.currentItem);
        }


        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, WidgetProvider.class));
        int appWidgetId;
        for (int i = 0; i < appWidgetIds.length; i++) {
            appWidgetId = appWidgetIds[i];


            RemoteViews views = getCurrentLayout(context, appWidgetManager, appWidgetId);
            if (RemoteFetchService.oneList != null && RemoteFetchService.oneList.size() != 0) {
                Listsubitem s = RemoteFetchService.oneList.get(RemoteFetchService.currentItem);
                views.setTextViewText(R.id.title, s.title);
                views.setTextViewText(R.id.string, s.string);
                views.setTextViewText(R.id.substring, s.substring);
            }
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }


    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        Log.i(TAG, "onAppWidgetOptionsChanged");

        RemoteViews views = getCurrentLayout(context, appWidgetManager, appWidgetId);
        appWidgetManager.updateAppWidget(appWidgetId, views);

        int[] id1 = {appWidgetId};
        onUpdate(context, appWidgetManager, id1);

        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    }





    private RemoteViews getCurrentLayout(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {;
        int out;
        int rows = getCellsForSize(appWidgetManager.getAppWidgetOptions(appWidgetId).getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT));
        if (rows == 1) {
            out = R.layout.stat_widget_small;
        } else {
            out = R.layout.stat_widget;
        }
        RemoteViews views = new RemoteViews(context.getPackageName(), out);
        Intent left = new Intent(context, WidgetProvider.class);
        left.setAction(WidgetProvider.ACTION_LEFT_BUTTON_CLICKED);
        left.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        PendingIntent leftPI = PendingIntent.getBroadcast(context, 0, left, 0);
        views.setOnClickPendingIntent(R.id.left_button, leftPI);

        Intent right = new Intent(context, WidgetProvider.class);
        right.setAction(WidgetProvider.ACTION_RIGHT_BUTTON_CLICKED);
        right.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        PendingIntent rightPI = PendingIntent.getBroadcast(context, 0, right, 0);
        views.setOnClickPendingIntent(R.id.right_button, rightPI);
        return views;
    }



    private static int getCellsForSize(int size) {
        int n = 2;
        while (70 * n - 30 < size) {
            ++n;
        }
        return n - 1;
    }


}

