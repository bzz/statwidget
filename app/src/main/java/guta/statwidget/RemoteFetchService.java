package guta.statwidget;


import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import guta.statwidget.manager.ConnectionManager;
import guta.statwidget.manager.FileManager;


public class RemoteFetchService extends Service {


    public final static String TAG = "RemoteFetchService";
    public static ArrayList<Listsubitem> oneList;
    public static int currentItem = 0;

    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");

        if (intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID))
            appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);

        fetchDataFromWeb();

        return super.onStartCommand(intent, flags, startId);
    }



    public void fetchDataFromWeb() {
        Log.i(TAG, "fetchDataFromWeb");


        String result = "";

/* TODO: Remove the hack making possible internet connections from the main thread */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if ( !(ConnectionManager.canConnectContext(getApplicationContext()))) return;
        result = ConnectionManager.readFromURL(ConnectionManager.SERVER_URL).trim();

///get json from file
//        result = FileManager.readStream(getBaseContext().getResources().openRawResource(R.raw.json_example));

        Log.i(TAG, "RESULT.trim(): " + result);
        if (result != null) {
            FileManager.writeToFile(getApplicationContext(), result, "text.json");
        } else {
            result = FileManager.readFromFile(getApplicationContext(), "text.json");
        }
        processResult(result);

    }


    private void processResult(String result) {
        Log.i(TAG, "processResult");
        oneList = new ArrayList<Listsubitem>();

        try {
            JSONObject obj = new JSONObject(result);
            for(Iterator<String> keys=obj.keys();keys.hasNext();) {
                String title = keys.next().toString();
                JSONArray jsonArray =  obj.getJSONArray(title);

                Log.i(TAG, "jsonArray  " + jsonArray.toString());


                for(int n = 0; n < jsonArray.length(); n++) {
                    JSONObject object = jsonArray.getJSONObject(n);
                    
                    
                  //  oneList.add(item);


                    Log.i(TAG, "object " + object.toString());
//
                    if (object.has("title") && object.has("string")) {

                        Listsubitem subitem = new Listsubitem(
                                object.getString("title"),
                                object.getString("string"),
                                object.getString("substring"));
                        Log.i(TAG, "subitem " + subitem.toString());
                        oneList.add(subitem);
                    }
                }
            }
        } catch (JSONException e) {e.printStackTrace();}

        Log.i(TAG, "oneList: " + oneList.toString());

        populateWidget();
    }


    private void populateWidget() {
        Intent updateIntent = new Intent();
        updateIntent.setAction(WidgetProvider.DATA_FETCHED);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        sendBroadcast(updateIntent);
        this.stopSelf();
    }





}
