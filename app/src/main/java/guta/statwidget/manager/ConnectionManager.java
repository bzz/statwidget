package guta.statwidget.manager;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;


public enum ConnectionManager {
    INSTANCE;

    public static final String SERVER_HOST =  "rpix.herokuapp.com";//"btc-e.com/api/3/ticker/btc_rur"; //"rpix.herokuapp.com"; //
    public static final String SERVER_URL = "http://" + SERVER_HOST;
    public static final int SERVER_PORT = 80;


    public static boolean canConnectContext(Context context) {
        try {
            if ( !(ConnectionManager.isConnected(context)) ) {
                Toast.makeText(context, "CONNECTION ERROR", Toast.LENGTH_SHORT).show();
                return false;
            } else if ( !(ConnectionManager.isHostReachable(ConnectionManager.SERVER_HOST, ConnectionManager.SERVER_PORT, 20000)) ) {
                Toast.makeText(context, "SERVER NOT REACHABLE", Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        } catch (Exception e) {Log.e("Exception", "canConnectContext " + e.toString());
            return false;
        }
    }

    public static String readFromURL(String urlStr) {
        String out = new String();
        try {
            URL url = new URL(urlStr);
            URLConnection urlConnection = url.openConnection();
            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
                for (String line = r.readLine(); line != null; line =r.readLine()){
                    sb.append(line);
                }
            } finally {
                is.close();
                out = sb.toString();
            }
        } catch (IOException e) {Log.e("Exception", "readFromURL " + e.toString());}
        return out;
    }


    private static boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isHostReachable(String serverAddress, int serverTCPport, int timeoutMS) {
        boolean connected = false;
        Socket socket;
        try {
            socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(serverAddress, serverTCPport);
            socket.connect(socketAddress, timeoutMS);
            if (socket.isConnected()) {
                connected = true;
                socket.close();
            }
        } catch (IOException e) {Log.e("Exception", "isHostReachable " + e.toString());}
        return connected;
    }

}
