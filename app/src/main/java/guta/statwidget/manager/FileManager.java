package guta.statwidget.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import android.content.Context;
import android.util.Log;


public enum FileManager {
    INSTANCE;

    static public void writeToFile(Context context, String text, String fileName) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(text);
            outputStreamWriter.close();
        } catch (IOException e) {Log.e("Exception", "writeToFile " + e.toString());}
    }

    static public String readFromFile(Context context, String fileName) {
        File file = new File(context.getFilesDir(), fileName);
        int length = (int) file.length();
        byte[] bytes = new byte[length];
        try {
            FileInputStream in = new FileInputStream(file);
            try {
                in.read(bytes);
            } finally {
                in.close();
            }
        } catch (IOException e) {Log.e("Exception", "readFromFile " + e.toString());}
        return new String(bytes);
    }



}

